var Game={};

Game.preload = function() {
	game.load.image('sky','/assets/sky.png');
	game.load.image('ground', '/assets/platform.png');
	game.load.image('star','/assets/star.png');
	game.load.spritesheet('dude','/assets/dude.png',32,48);
};

var sprite;
var platforms;
var player;
var cursors;
var stars;
var score=0;
var scoreText;

Game.create=function() {
	Game.playerMap = {};

	var testKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
	testKey.onDown.add(Client.sendTest, this);

	//plein écran
	game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
	game.scale.pageAlignVertically = true;
	game.scale.pageAlignHorizontally = true;
	game.scale.setScreenSize( true );

//taille niveau
  game.world.setBounds(0,0,6000,1000);

	game.physics.startSystem(Phaser.Physics.ARCADE);
	game.add.tileSprite(0,0,6000,1000,'sky');
	platforms = game.add.group();
	platforms.enableBody =true;
	var ground;
	for(var i=0; i<6000;i+=600){
		ground = platforms.create(i,game.world.height - 64,'ground');
		ground.scale.setTo(2,2);
		ground.body.immovable=true;
	}

	var ledge= platforms.create(300,810,'ground');
	ledge.body.immovable=true;
	ledge = platforms.create(-150,680,'ground');
	ledge.body.immovable=true;

	player=game.add.sprite(32,game.world.height-150,'dude');
	game.physics.arcade.enable(player);
	player.body.bounce.y=0;
	player.body.gravity.y=3000;
	player.body.collideWorldBounds=true;
	player.animations.add('left',[0,1,2,3],10,true);
	player.animations.add('right',[5,6,7,8],10,true);
  game.camera.follow(player,Phaser.Camera.FOLLOW_LOCKON);

	stars = game.add.group();
	stars.enableBody = true;
	for(i=0;i<12; i++)
	{
		var star = stars.create(i*70,0,'star');
		star.body.gravity.y=500;
		star.body.bounce.y = 0.7 + Math.random() * 0.2;
	}
	//scoreText = game.add.text(16,16, 'score : 0',{fontSize:'32px',fill:'#000'});
	//scoreText.fixedToCamera=true;
	cursors = game.input.keyboard.createCursorKeys();
	getCoordinates(32,game.world.height-150);
	Client.askNewPlayer();
};
function getCoordinates (x,y){
    Client.sendClick(x,y);
}

Game.addNewPlayer = function(id,x,y){
    Game.playerMap[id] = game.add.sprite(x,y,'sprite');
};

Game.movePlayer = function(id,x,y){
    var player = Game.playerMap[id];
    var distance = Phaser.Math.distance(player.x,player.y,x,y);
    var tween = game.add.tween(player);
    var duration = distance*10;
    tween.to({x:x,y:y}, duration);
    tween.start();
};

Game.removePlayer = function(id){
    Game.playerMap[id].destroy();
    delete Game.playerMap[id];
};

Game.update = function() {

	var hitPlatform = game.physics.arcade.collide(player, platforms);
	game.physics.arcade.collide(stars,platforms);
	game.physics.arcade.overlap(player,stars,collectStar,null,this);
	game.physics.arcade.collide(player,platforms);
	player.body.velocity.x=0;
	if(cursors.left.isDown)
	{
		player.body.velocity.x=-300;
		player.animations.play('left');

	}else if (cursors.right.isDown) {
		player.body.velocity.x=300;
		player.animations.play('right');
	}else {
		player.animations.stop();
		player.frame=4;
	}
	 // saut de 130 y.
	if(cursors.up.isDown && player.body.touching.down && hitPlatform){
		player.body.velocity.y=-900;
	}
	if(cursors.down.isDown && !player.body.touching.down)
	{
		player.body.velocity.y=900;
	}

};
function collectStar(player,star){
  star.kill();
	score+=100;
	//scoreText.text = 'Score : ' + score;

}
Game.render=function(){
	game.debug.cameraInfo(game.camera, 32, 32, "#000000");

  game.debug.pointer( game.input.activePointer,false,null,null,"#0000000" );

};

var game = new Phaser.Game(2000, 800, Phaser.AUTO, document.getElementById('game'));
game.state.add('Game',Game);
game.state.start('Game');
